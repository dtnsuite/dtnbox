/********************************************************
  ** Authors: Nicolò Castellazzi, nicolo.castellazzi@studio.unibo.it
  **          Marcello Ballanti, marcello.ballanti@studio.unibo.it
  **          Marco Bertolazzi, marco.bertolazzi3@studio.unibo.it
  **          Carlo Caini (DTNbox project supervisor), carlo.caini@unibo.it
  **
  ** Copyright 2018 University of Bologna
  ** Released under GPLv3. See LICENSE.txt for details.
  **
  ********************************************************/

/*
 * al_bp_wrapper.c
 * Modificato da Caini per supportare lo schema ipn e l'implementazione IBR
 * -modifica con Andrea Bisacchi per usare l'al_socket
 * -modificato provvisoriamente path del tar da inviare nel bundle per presunto bug (percorso < 31) in caso di ION.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "al_bp_wrapper.h"
#include "utils.h"
#include <libgen.h>

#include "../../unified_api/src/al/bundle/al_bundle.h"
#include "../../unified_api/src/al/socket/al_socket.h"
#include "../../unified_api/src/al/utilities/debug_print/al_utilities_debug_print.h"
#include "../../unified_api/src/al_bp/al_bp.h"
#include "../../unified_api/src/al/bundle/options/al_bundle_options.h"
#include "../Model/definitions.h"
// 0 -> tutto ok
int dtnbox_openConnection(
		al_socket_registration_descriptor* register_descriptor,
		char* demux_token_ipn, char* demux_token_dtn, dtn_suite_options_t dtn_suite_options) {

	al_error error_socket;
	//TODO: la register deve essre aggiornata opportunamente con i dati ottenuti attraverso il parser (vedi dtnperf main)
	//NB: none indica di fare la open normale senza indirizzo ip (vedi parser al_bp)
	//TODO: i parametri force_id e ipn_number devono essere dati al posto di N e 0
	error_socket = al_socket_register(register_descriptor, demux_token_dtn, atoi(demux_token_ipn),'N',0, "none", 0);
	return error_socket;
}

int dtnbox_closeConnection(
		al_socket_registration_descriptor register_descriptor) {

	al_error socket_error;

	socket_error = al_socket_unregister(register_descriptor);
	if (socket_error != AL_SUCCESS) {
		al_utilities_debug_print_error(
				"dtnbox_closeConnection: error in al_socket_unregister() (%d)\n",
				socket_error);
		return ERROR_VALUE;
	}
	return SUCCESS_VALUE;
}

int dtnbox_send(al_socket_registration_descriptor register_descriptor,
		char* tarName, dtnNode dest, bundle_options_t bundle_options) {

	int syserr;
	al_error error;
	al_types_bundle_object bundle;
	al_types_bundle_payload_location location = BP_PAYLOAD_FILE;
	al_types_bundle_priority_enum cardinal_priority;
	uint32_t ordinal_priority;
	al_types_timeval bundle_expiration;

	al_types_endpoint_id to;
	al_types_endpoint_id report_to;

	char cpCommand[SYSTEM_COMMAND_LENGTH + 2*DTNBOX_SYSTEM_FILES_ABSOLUTE_PATH_LENGTH];
	char newPath[DTNBOX_SYSTEM_FILES_ABSOLUTE_PATH_LENGTH];


	getHomeDir(newPath);
	strcat(newPath, basename(tarName));
	strcpy(cpCommand, "cp \"");
	strcat(cpCommand, tarName);
	strcat(cpCommand, "\" \"");
	strcat(cpCommand, newPath);
	strcat(cpCommand, "\"");

	syserr = system(cpCommand);
	if (syserr) {
		al_utilities_debug_print_error("dtnbox_send: error in system(%s)\n",
				cpCommand);
		return ERROR_VALUE;
	}

	cardinal_priority = BP_PRIORITY_NORMAL;
	ordinal_priority = 0;
	bundle_expiration = (al_types_timeval) dest.lifetime;

	//creo bundle
	error = al_bundle_create(&bundle);
	if (error != AL_SUCCESS) {
		return ERROR_VALUE;
	}
	error = al_bundle_set_payload_location(&bundle, location);
	if (error != AL_SUCCESS) {
		al_bundle_free(&bundle);
		return ERROR_VALUE;
	}

	error = al_bundle_set_payload_file(&bundle, newPath, strlen(newPath));
	//error = al_bundle_set_payload_file(&bundle, tarName, strlen(tarName));
	if (error != AL_SUCCESS) {
		al_bundle_free(&bundle);
		return ERROR_VALUE;
	}
	//al_bundle_options_set(al_types_bundle_object *bundle,bundle_options_t bundle_options)
	error = al_bundle_set_cardinal_priority(&bundle, cardinal_priority);
	error = al_bundle_set_ordinal_priority(&bundle, ordinal_priority);

	if (error != AL_SUCCESS) {
		al_bundle_free(&bundle);
		return ERROR_VALUE;
	}
	bundle.spec->ecos.ordinal = 0;
	bundle.spec->ecos.critical = FALSE;
	bundle.spec->ecos.flow_label = 0;
	bundle.spec->ecos.unreliable = FALSE;

	error = al_bundle_set_lifetime(&bundle, bundle_expiration);
	if (error != AL_SUCCESS) {
		al_bundle_free(&bundle);
		return ERROR_VALUE;
	}
	error = al_bundle_set_control_flags(&bundle,
			(al_types_bundle_processing_control_flags) 0);
	if (error != AL_SUCCESS) {
		al_bundle_free(&bundle);
		return ERROR_VALUE;
	}

	//al_bp_parse_eid_string(&to, dest.EID);
	strcpy(to.uri,dest.EID);
	//al_bp_get_none_endpoint(&report_to);
	strcpy(report_to.uri, "dtn:none");
	error = al_socket_send(register_descriptor, bundle, to,
			report_to);
	if (error != AL_SUCCESS) {
		al_utilities_debug_print_error("dtnbox_send: error in al_socket_send_bundle(): %s\n",
				al_socket_str_type_error(error));
		al_bundle_free(&bundle);
		return ERROR_VALUE;
	}

	//mandato il bundle, lo dealloco
	error = al_bundle_free(&bundle);
	if (error != AL_SUCCESS) {
		al_utilities_debug_print_error("dtnbox_send: error in al_bundle_free(): \n");
		al_bundle_free(&bundle);
		return ERROR_VALUE;
	}
	return SUCCESS_VALUE;
}

int dtnbox_receive(al_socket_registration_descriptor register_descriptor,
		char* tarName, char* sourceEID) {
	al_error error;
	uint32_t fileNameLen;
	al_types_bundle_object bundle;
	al_types_bundle_payload_location location = BP_PAYLOAD_FILE;
	al_types_endpoint_id sourceEIDStruct;
	char* fileName;

	//creo l'oggetto bundle
	error = al_bundle_create(&bundle);
	if (error != AL_SUCCESS) {
		al_utilities_debug_print_error("dtnbox_receive: error in al_bundle_create \n");
		return ERROR_VALUE;
	}

	//aspetto di riceverlo
	al_utilities_debug_print(DEBUG_L1,"dtnbox_receive: waiting for a bundle...\n");
	error = al_socket_receive(register_descriptor, &bundle,
			location, -1);
	if (error != AL_SUCCESS) {
		switch(error){
		case AL_ERROR:{
			//ERROR
			al_utilities_debug_print_error("dtnbox_receive: error AL_ERROR in al_socket_receive_bundle()\n");
			al_bundle_free(&bundle);
			return ERROR_VALUE;
		}
		case AL_WARNING_TIMEOUT:{
			//WARNING
			al_utilities_debug_print(DEBUG_L2, "Warning in al_socket_receive: Timeout \n");
			al_bundle_free(&bundle);
			return WARNING_VALUE;
		}
		case AL_WARNING_RECEPINTER:{
			//WARNING
			al_utilities_debug_print(DEBUG_L2, "Warning in al_socket_receive: Reception is interrupted \n");
			al_bundle_free(&bundle);
			return WARNING_VALUE;
		}
		default:{
			//ERROR
			al_utilities_debug_print_error("dtnbox_receive: error in al_socket_receive_bundle()\n");
			al_bundle_free(&bundle);
			return ERROR_VALUE;
		}
		}//switch
	}//if

	//dal bundle costruisco il tar e ricavo il mittente
	error = al_bundle_get_payload_file(bundle, &fileName, &fileNameLen);
	if (error != AL_SUCCESS) {
		al_utilities_debug_print_error(
				"dtnbox_receive: error in al_bundle_get_payload_file() \n");
		al_bundle_free(&bundle);
		return ERROR_VALUE;
	}
	strcpy(tarName, fileName);

	//stampa per capire dov'e' il file con il contenuto del bundle
	//al_utilities_debug_print(DEBUG_L1, "dtnbox_receive: received bundle, associated file=%s\n", fileName);

	error = al_bundle_get_source(bundle, &sourceEIDStruct);
	if (error != AL_SUCCESS) {
		al_bundle_free(&bundle);
		return ERROR_VALUE;
	}
	strcpy(sourceEID, sourceEIDStruct.uri);

	//libero memoria dal nome file. Suppongo sia necessario per come viene usata la funzione al_bundle_get_payload_file().
	free(fileName);

	//distruggo il bundle
	error = al_bundle_free(&bundle);
	if (error != AL_SUCCESS) {
		al_utilities_debug_print_error("dtnbox_receive: error in al_bundle_free()\n");
		al_bundle_free(&bundle);
		return ERROR_VALUE;
	}
	return SUCCESS_VALUE;
}
