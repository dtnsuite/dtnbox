/********************************************************
  ** Authors: Nicolò Castellazzi, nicolo.castellazzi@studio.unibo.it
  **          Marcello Ballanti, marcello.ballanti@studio.unibo.it
  **          Marco Bertolazzi, marco.bertolazzi3@studio.unibo.it
  **          Carlo Caini (DTNbox project supervisor), carlo.caini@unibo.it
  **
  ** Copyright 2018 University of Bologna
  ** Released under GPLv3. See LICENSE.txt for details.
  **
  ********************************************************/

/*
 * al_bp_wrapper.h
 */

#ifndef AL_BP_WRAPPER_H_
#define AL_BP_WRAPPER_H_

#include "../../unified_api/src/al/bundle/al_bundle.h"
#include "../../unified_api/src/al/socket/al_socket.h"
#include "../../unified_api/src/al/types/al_types.h"
#include "../../unified_api/src/al_bp/al_bp.h"
#include "../Model/definitions.h"
#include "../../unified_api/src/al/bundle/options/al_bundle_options.h"
//creazione connessione
int dtnbox_openConnection(al_socket_registration_descriptor* register_descriptor, char* demux_token_ipn, char* demux_token_dtn,dtn_suite_options_t dtn_suite_options);
//invio messaggio
int dtnbox_send(al_socket_registration_descriptor register_descriptor, char* tarName, dtnNode dest, bundle_options_t bundle_options);
//ricezione messaggio. tempo 0 = aspetta all'infinito.
int dtnbox_receive(al_socket_registration_descriptor register_descriptor, char* tarName, char* sourceEID);
//chiusura connessione
int dtnbox_closeConnection(al_socket_registration_descriptor register_descriptor);

#endif /* AL_BP_WRAPPER_H_ */
